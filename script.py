import jenkinsapi
from jenkinsapi import api
import datetime
from datetime import datetime
import sqlite3

# get jenkins server instance
def get_server_instance():
    jenkins_url = 'https://ci.jenkins.io/'
    server = api.Jenkins(jenkins_url)
    print "Successfully connected to jenkins server"
    return server

# get db connection, in this case, scplite
def get_db_conn():
    conn = sqlite3.connect('example.db')
    return conn

# retrieves details for a particular job
def get_job_details(job, sql, c):
    try:
        print "processing " + job[0]
        j = job[1]
        build = j.get_last_build()
        c.execute(sql, (job[0], build.get_status(), datetime.now()))
    except:
        "error processing " + job[0]
    return c

# create a table in database
def create_db_table():
    conn = get_db_conn()
    conn.execute('''CREATE TABLE IF NOT EXISTS jobs (id INT PRIMARY KEY, job_name CHAR(200), job_status CHAR(50), job_check_time CHAR(50))''')
    print "Table created successfully";
    conn.close()

# start executing main script
# first setup db and create table
# then get a curso to the db
# then retrieve all jobs from server
# iterate through jobs and get details for each and insert into db
# At the end of loop commit all changes to the db.
# finished

print "START..."
create_db_table()
conn = get_db_conn()
cur = conn.cursor()
print "Database connection established..."
sql = "INSERT INTO jobs (job_name, job_status, job_check_time) VALUES(?,?,?)"
server = get_server_instance()
jobs = server.get_jobs()
print "Retrieved all jobs from the server \n"
print "\nEntering loop now... \n"
for j in jobs:
    cur = get_job_details(j, sql, cur)
    print "Processed job: "+ j[0] + "..."
print "\n Exited loop..."
conn.commit()
print "database changes saved..."
print "ALL DONE!!! \n Thanks."
