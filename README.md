# README #

This is python script to retrieve the list of jobs from a jenkins server and save the job status and date retrieved for each job an sqlite database.

The script is written and tested with python 2.7.

### Assumptions ###

* The script connects to a jenkins server on https://ci.jenkins.io
* The script creates an sqlite database on example.db on the same folder as the script
* The script makes use of the following python libraries/modules
	`jenkinsapi`, `datetime`, and `sqlite3`

#### Okezie Arukwe okeziearukwe@gmail.com ####